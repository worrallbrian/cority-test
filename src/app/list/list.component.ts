import { Component, OnInit } from "@angular/core";
import { Item } from "../item";
import { SAMPLEITEMS } from "../sample-items";
import { trigger, state, style, animate, transition } from "@angular/animations";

@Component({
	selector: "app-list",
	templateUrl: "./list.component.html",
	styleUrls: ["./list.component.css"],
	animations: [
		trigger("openClose", [
			state("open", style({ height: "*" })),
			state("closed", style({ height: "0px" })),
			transition("closed <=> open", animate(500))
		])
	]
})

export class ListComponent implements OnInit {
	state = "open";
	timeOutRef;
	items = SAMPLEITEMS;
	totalNew = this.items.filter(item => item.new).length;
	selectedIndex = -1;

	constructor() { }

	ngOnInit() {}

	toggleState() {
		this.state = ((this.state === "open") ? "closed" : "open");
	}

	addItem() {
		console.log("Add item button hit!");
	}

	deleteItem(item) {
		this.items = this.items.filter(x => x !== item);
	}

	downloadItem(item) {
		console.log("Download button hit!");
	}

	showContent(evt, index) {
		this.selectedIndex = ((this.selectedIndex != index) ? index : -1);
	}
}
