import { Item } from "./item";

export const SAMPLEITEMS: Item[] = [{
	id: 1,
	new: true,
	title: "Lorem ipsum",
	icon: "https://cms-assets.tutsplus.com/uploads/users/392/posts/5585/preview_image/50-magnificent-landscapes.jpg",
	subtitle: "nisi ut aliquip ex ea commodo",
	description: "eprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
	date: new Date()
},{
	id: 2,
	new: false,
	title: "onsectetur adipiscing",
	icon: null,
	subtitle: "et dolore magna aliqua. Ut enim ad minim",
	description: "Excepteur sint occaecat cupidatat non proident, sunt in cul",
	date: new Date()
},{
	id: 3,
	new: false,
	title: "Lorem ipsum",
	icon: "http://www.freegreatpicture.com/files/photo100/49573-landscapes.jpg",
	subtitle: "nisi ut aliquip ex ea commodo",
	description: "eprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
	date: new Date()
}];
